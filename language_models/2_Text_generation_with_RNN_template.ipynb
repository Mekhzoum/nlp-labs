{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Text generation with RNN\n",
    "\n",
    "In this notebook, we will use RNN to generate text. The RNN will be trained to learn the character regularities of a corpus of text and use to generate sequences of character according to this character distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import random\n",
    "import rnn_utils  as rnn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The data\n",
    "\n",
    "We will train the RNN on a corpus of Romanian family names. Some examples extracted from the training file are :  \n",
    "\n",
    "* popa\n",
    "* popescu\n",
    "* pop\n",
    "* radu\n",
    "* dumitru\n",
    "\n",
    "We first load the data and check the number of characters. We keep '\\n' as a maker of the end of the sequence."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_file = 'data/romanian_names.txt'\n",
    "data = open(data_file, 'r').read()\n",
    "chars = list(set(data))\n",
    "data_size, vocab_size = len(data), len(chars)\n",
    "print('There are %d total characters and %d unique characters in your data.' % (data_size, vocab_size))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We create dictionaries to convert characters to indexes and indexes to characters : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "char_to_ix = {ch: i for i, ch in enumerate(sorted(chars))}\n",
    "ix_to_char = {i: ch for i, ch in enumerate(sorted(chars))}\n",
    "print(ix_to_char)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define some useful functions : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from numpy.testing import *\n",
    "# define some utils functions\n",
    "\n",
    "def softmax(x):\n",
    "    e_x = np.exp(x - np.max(x))\n",
    "    return e_x / e_x.sum(axis=0)\n",
    "\n",
    "\n",
    "def sigmoid(x):\n",
    "    return 1 / (1 + np.exp(-x))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generating characters with RNN\n",
    "\n",
    "We assume that the RNN model is trained. We would like to generate new text (characters). The process of generation is explained in the picture below:\n",
    "\n",
    "<img src=\"./images/lab_rnn3.png\" style=\"width:1000px\" >\n",
    "\n",
    "<caption><center> Figure 3: In this picture, we assume the model is already trained. We pass in $x^{\\langle 1\\rangle} = \\vec{0}$ at the first time step, and have the network then sample one character at a time. </center></caption>\n",
    "\n",
    "**Exercise**: Implement the `sample` function below to sample characters. You need to carry out 4 steps:\n",
    "\n",
    "- **Step 1**: Pass the network the first \"dummy\" input $x^{\\langle 1 \\rangle} = \\vec{0}$ (the vector of zeros). This is the default input before we've generated any characters. We also set $a^{\\langle 0 \\rangle} = \\vec{0}$\n",
    "\n",
    "- **Step 2**: Run one step of forward propagation to get $a^{\\langle 1 \\rangle}$ and $\\hat{y}^{\\langle 1 \\rangle}$. Here are the equations:\n",
    "\n",
    "$$ a^{\\langle t+1 \\rangle} = \\tanh(W_{ax}  x^{\\langle t+1 \\rangle } + W_{aa} a^{\\langle t \\rangle } + b)\\tag{1}$$\n",
    "\n",
    "$$ z^{\\langle t + 1 \\rangle } = W_{ya}  a^{\\langle t + 1 \\rangle } + b_y \\tag{2}$$\n",
    "\n",
    "$$ \\hat{y}^{\\langle t+1 \\rangle } = softmax(z^{\\langle t + 1 \\rangle })\\tag{3}$$\n",
    "\n",
    "Note that $\\hat{y}^{\\langle t+1 \\rangle }$ is a (softmax) probability vector (its entries are between 0 and 1 and sum to 1). $\\hat{y}^{\\langle t+1 \\rangle}_i$ represents the probability that the character indexed by \"i\" is the next character.  We have provided a `softmax()` function that you can use.\n",
    "\n",
    "- **Step 3**: Carry out sampling: Pick the next character's index according to the probability distribution specified by $\\hat{y}^{\\langle t+1 \\rangle }$. This means that if $\\hat{y}^{\\langle t+1 \\rangle }_i = 0.16$, you will pick the index \"i\" with 16% probability. To implement it, you can use [`np.random.choice`](https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.random.choice.html).\n",
    "\n",
    "Here is an example of how to use `np.random.choice()`:\n",
    "```python\n",
    "np.random.seed(0)\n",
    "p = np.array([0.1, 0.0, 0.7, 0.2])\n",
    "index = np.random.choice([0, 1, 2, 3], p = p.ravel())\n",
    "```\n",
    "This means that you will pick the `index` according to the distribution: \n",
    "$P(index = 0) = 0.1, P(index = 1) = 0.0, P(index = 2) = 0.7, P(index = 3) = 0.2$.\n",
    "\n",
    "- **Step 4**: The last step to implement in `sample()` is to overwrite the variable `x`, which currently stores $x^{\\langle t \\rangle }$, with the value of $x^{\\langle t + 1 \\rangle }$. You will represent $x^{\\langle t + 1 \\rangle }$ by creating a one-hot vector corresponding to the character you've chosen as your prediction. You will then forward propagate $x^{\\langle t + 1 \\rangle }$ in Step 1 and keep repeating the process until you get a \"\\n\" character, indicating you've reached the end of the sequence. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "def sample(parameters, char_to_ix):\n",
    "    \"\"\"\n",
    "    Sample a sequence of characters according to a sequence of probability distributions output of the RNN\n",
    "\n",
    "    params: parameters -- python dictionary containing the parameters Waa, Wax, Wya, by, and b. \n",
    "    params: char_to_ix -- python dictionary mapping each character to an index.\n",
    "\n",
    "    returns : indices -- a list of length n containing the indices of the sampled characters.\n",
    "    \"\"\"\n",
    "    \n",
    "    MAX_LENGTH = 10\n",
    "    # Retrieve parameters and relevant shapes from \"parameters\" dictionary\n",
    "    Waa, Wax, Wya, by, b = parameters['Waa'], parameters['Wax'], parameters['Wya'], parameters['by'], parameters['b']\n",
    "    vocab_size = by.shape[0]\n",
    "    n_a = Waa.shape[1]\n",
    "    \n",
    "    \n",
    "    # Step 1: Create the one-hot vector x for the first character (initializing the sequence generation). \n",
    "    x =  # YOUR CODE HERE\n",
    "\n",
    "    init_idx = np.random.choice(np.arange(vocab_size))\n",
    "    x[init_idx] = 1\n",
    "    # Step 1': Initialize a_prev as zeros \n",
    "    a_prev = # YOUR CODE HERE\n",
    "    \n",
    "    # Create an empty list of indices, this is the list which will contain the list of indices of the characters to generate \n",
    "    indices = []\n",
    "    \n",
    "    # Idx is a flag to detect a newline character, we initialize it to -1\n",
    "    idx = -1 \n",
    "    \n",
    "    # Loop over time-steps t. At each time-step, sample a character from a probability distribution and append \n",
    "    # its index to \"indices\". We'll stop if we reach 10 characters (which should be very unlikely with a well \n",
    "    # trained model), which helps debugging and prevents entering an infinite loop. \n",
    "    counter = 0\n",
    "    newline_character = char_to_ix['\\n']\n",
    "    \n",
    "    while (idx != newline_character and counter != MAX_LENGTH):\n",
    "        \n",
    "        # Step 2: Forward propagate x using the equations (1), (2) and (3)\n",
    "        a =   # YOUR CODE HERE\n",
    "        z =  # YOUR CODE HERE\n",
    "        y =   # YOUR CODE HERE\n",
    " \n",
    "\n",
    "        # Step 3: Sample the index of a character within the vocabulary from the probability distribution y\n",
    "        idx =   # YOUR CODE HERE\n",
    "\n",
    "        # Append the index to \"indices\"\n",
    "        indices.append(idx)\n",
    "        \n",
    "        # Step 4: Overwrite the input character as the one corresponding to the sampled index.\n",
    "        # create a vector of zeros \n",
    "        x =   # YOUR CODE HERE\n",
    "        # set to 1 the indice corresponding to the predicted word\n",
    "        x[idx] = # YOUR CODE HERE\n",
    "        \n",
    "        # Update \"a_prev\" to be \"a\"\n",
    "        a_prev = a\n",
    "\n",
    "    if (counter == MAX_LENGTH):\n",
    "        indices.append(char_to_ix['\\n'])\n",
    "    \n",
    "    return indices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test your sampling function with a random neural network : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(10)\n",
    "_, n_a = 20, 100\n",
    "\n",
    "Wax, Waa, Wya = np.random.randn(n_a, vocab_size), np.random.randn(n_a, n_a), np.random.randn(vocab_size, n_a)\n",
    "b, by = np.random.randn(n_a, 1), np.random.randn(vocab_size, 1)\n",
    "parameters = {\"Wax\": Wax, \"Waa\": Waa, \"Wya\": Wya, \"b\": b, \"by\": by}\n",
    "\n",
    "\n",
    "indices = sample(parameters, char_to_ix)\n",
    "print(\"Sampling:\")\n",
    "print(\"list of sampled indices:\", indices)\n",
    "print(\"list of sampled characters:\", [ix_to_char[i] for i in indices])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training the model\n",
    "\n",
    "Given the dataset of sequences, we use each line of the dataset (one example) as one training example. Every 2000 steps of stochastic gradient descent, you will sample 10 randomly chosen names to see how the algorithm is doing. Remember to shuffle the dataset, so that stochastic gradient descent visits the examples in random order. \n",
    "\n",
    "**Exercise**: Follow the instructions and implement `model()`. When `examples[index]` contains one sample (string), to create an example (X, Y), you can use this:\n",
    "```python\n",
    "        index = j % len(examples)\n",
    "        X = [None] + [char_to_ix[ch] for ch in examples[index]] \n",
    "        Y = X[1:] + [char_to_ix[\"\\n\"]]\n",
    "```\n",
    "Note that we use: `index= j % len(examples)`, where `j = 1....num_iterations`, to make sure that `examples[index]` is always a valid statement (`index` is smaller than `len(examples)`).\n",
    "The first entry of `X` being `None` will be interpreted by `rnn_forward()` as setting $x^{\\langle 0 \\rangle} = \\vec{0}$. Further, this ensures that `Y` is equal to `X` but shifted one step to the left, and with an additional \"\\n\" appended to signify the end of the sequence. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def model(data, ix_to_char, char_to_ix, num_iterations = 2001, n_a = 2, num_samples = 10, vocab_size = 40):\n",
    "    \"\"\"\n",
    "    Trains the model and generates sequences of characters. \n",
    "    \n",
    "    params: data -- text corpus\n",
    "    params: ix_to_char -- dictionary that maps the index to a character\n",
    "    params: char_to_ix -- dictionary that maps a character to an index\n",
    "    params: num_iterations -- number of iterations to train the model for\n",
    "    params: n_a -- number of units of the RNN cell\n",
    "    params: num_samples -- number of examples you want to sample at each iteration. \n",
    "    params: vocab_size -- number of unique characters found in the text, size of the vocabulary\n",
    "    \n",
    "    returns: parameters -- learned parameters\n",
    "    \"\"\"\n",
    "    \n",
    "    # Retrieve n_x and n_y from vocab_size\n",
    "    n_x, n_y = vocab_size, vocab_size\n",
    "    \n",
    "    # Initialize parameters\n",
    "    parameters = rnn.initialize_parameters(n_a, n_x, n_y)\n",
    "    \n",
    "    # Initialize loss \n",
    "    loss = rnn.get_initial_loss(vocab_size, num_samples)\n",
    "    \n",
    "    # Build list of all samples (training examples).\n",
    "    with open(data_file) as f:\n",
    "        examples = f.readlines()\n",
    "    examples = [x.lower().strip() for x in examples]\n",
    "    \n",
    "    # Shuffle list of all samples\n",
    "    np.random.seed(0)\n",
    "    np.random.shuffle(examples)\n",
    "    \n",
    "    # Initialize the hidden state of your LSTM\n",
    "    a_prev = np.zeros((n_a, 1))\n",
    "    \n",
    "    # Optimization loop\n",
    "    for j in range(num_iterations):\n",
    "        \n",
    "        \n",
    "        # Use the hint above to define one training example (X,Y) (≈ 2 lines)\n",
    "        index = # YOUR CODE HERE\n",
    "        X = # YOUR CODE HERE\n",
    "        Y = # YOUR CODE HERE\n",
    "        \n",
    "        # Perform one optimization step: Forward-prop -> Backward-prop -> Clip -> Update parameters\n",
    "        # Choose a learning rate of 0.01 and the size of the vocabulary in argument of the model function\n",
    "        curr_loss, gradients, a_prev = rnn.optimize(# YOUR CODE HERE)\n",
    "                \n",
    "        # Use a latency trick to keep the loss smooth. It happens here to accelerate the training.\n",
    "        loss = rnn.smooth(loss, curr_loss)\n",
    "\n",
    "        # Every 2000 Iteration, generate \"n\" characters thanks to sample() to check if the model is learning properly\n",
    "        if j % 2000 == 0:\n",
    "            \n",
    "            print('Iteration: %d, Loss: %f' % (j, loss) + '\\n')\n",
    "            \n",
    "            # The number of samples  to print\n",
    "            count = 0\n",
    "            while (count<num_samples):\n",
    "                # Sample indices and print them\n",
    "                sampled_indices = sample(parameters, char_to_ix)\n",
    "                if len(sampled_indices)>5:\n",
    "                    rnn.print_sample(sampled_indices, ix_to_char)\n",
    "                    count+=1\n",
    "\n",
    "            print('\\n')\n",
    "        \n",
    "    return parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the following cell, you should observe your model outputting random-looking characters at the first iteration. \n",
    "\n",
    "Increase the number of iterations and change the number of units in the RNN cell. \n",
    "\n",
    "After a few thousand iterations, your model should learn to generate reasonable-looking names. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters = model(data, ix_to_char, char_to_ix,vocab_size=vocab_size)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
